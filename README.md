# ZeroTwoXCI
A homebrew application for the Nintendo Switch used to manage titles.

DISCLAIMER: I take zero responsibility for any bans, damage, nuclear explosions, or anything else the use of Tinfoil may cause. Proceed with caution!

## Installation
1. Install FS sig patches, there are two ways of doing this:
    - Using the latest [hekate](https://github.com/CTCaer/hekate) with the option ``kip1patch=nosigchk`` in ``hekate_ipl.ini``.
    - Using [ReiNX](https://github.com/Reisyukaku/ReiNX)
2. Place the tinfoil nro in the "switch" folder on your sd card, and run using the homebrew menu.

## Download
For the latest NSO go to releases

## Usage
### For NSPs
1. Place NSPs in ``/tinfoil/nsp`` or extracted NSPs in ``/tinfoil/extracted``.
2. Launch Tinfoil via the homebrew menu and install your NSPs.

### For XCIs
1. Place XCI in ``/tinfoil/zerotwoxci/nameofgame``` with the XCI named ``game.xci``
2. Launch Tinfoil via the homebrew menu and install your XCIs.

## Network Install Instructions
1. Download and install Python 3.6.6 from https://www.python.org/downloads/
2. Download remote_install_pc.py from https://github.com/Adubbz/Tinfoil/blob/master/tools/remote_install_pc.py by right clicking on Raw and clicking "Save link as..."
3. Open the Network Install menu in Tinfoil
4. Run ``remote_install_pc.py`` on your PC on the same network as your Switch with python ``remote_install_pc.py <switch ip> <nsp directory>``, or alternatively with no arguments for interactive mode.
5. ???
6. Profit

## Donate
This is a donation to Adubbz (The creator of original tinfoil) and if you enjoy using this software and you have money to spare I reccomend donating to him for the effort he has put into the original tinfoil.

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=TBB9Q9YL9AB74&lc=AU&item_name=Adubbz%27s%20Tools%20%26%20Game%20Mods&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)

## Credits
I'd like to thank the following people for their help or for using some of their code.

Roothorick for Dedbae and keeping me sane

Adubbz for tinfoil

SciresM for hactool code (specifically the hfs0 header functions)

The-4N for a hex to ascii converter

My mother Alicia for testing and being a good mother, infact the best mother someone could ask for