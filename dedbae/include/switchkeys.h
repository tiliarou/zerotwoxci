#pragma once

typedef struct {
    char titlekey[16];
    char out_titlekey[16];
    char titlekeks[5][16];
    char header[32];
    char application_key_area[4][16];
} switchkeys_t;

extern switchkeys_t switchkeys;

// Import keys from the keyfile. You can specify a filename to try first; if not specified, or the specified name
// is not found, it will try some standard/logical paths.
void switchkeys_load_keyfile(char* specified_keyfile);
