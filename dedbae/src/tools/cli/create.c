#include "actions.h"

#include "args.h"
#include "nca.h"
#include "pfs0.h"
#include "utils.h"

#include <string.h>
#include <stdio.h>

uint64_t cr_callback_read_stdio(char* buf, uint64_t offset, uint64_t size, void* data) {
	FILE* fd = data;
	fseek(fd, offset, SEEK_SET);
	fread(buf, 1, size, fd);
}

uint64_t cr_callback_write_stdio(char* buf, uint64_t offset, uint64_t size, void* data) {
	FILE* fd = data;
	fseek(fd, offset, SEEK_SET);
	fwrite(buf, 1, size, fd);
}

void act_create(char* nca_filename, char* section_filenames[], unsigned int num_sections) {
	nca_header_t header;
	
	if(cmdline_args.oldfile != NULL) {
		FILE* old_fd = fopen(cmdline_args.oldfile, "rb");
		
		char old_enc_header[ sizeof(nca_header_t) ];
		nca_header_t old_header;
		fread(old_enc_header, 1, sizeof(nca_header_t), old_fd);
		fclose(old_fd);
		
		nca_decrypt_header(&old_header, old_enc_header);
		
		nca_init_header(&header, &old_header);
	}
	else
		nca_init_header(&header, NULL);
	
	if(cmdline_args.location != NULL) {
		if( strcmp(cmdline_args.location, "console") == 0 )
			header.location = NCA_LOCATION_CONSOLE;
		else
			header.location = NCA_LOCATION_GAMECARD;
	}
	
	FILE* nca = fopen(nca_filename, "wb");
	
	for(int i=0; i<num_sections; i++) {
		FILE* section = fopen(section_filenames[i], "rb");
		
		nca_section_header_t* sec_hd = &header.section_headers[i];
		pfs0_header_t pfs_hd;
		
		fread(&pfs_hd, 1, sizeof(pfs0_header_t), section);
		if( strncmp(pfs_hd.magic, "PFS0", 4) != 0 )
			bail("Unrecognized file format. (supports PFS0 only for now)");
		
		if(i==0)
			header.section_table[i].media_offset = 6; // End of NCA header
		else
			header.section_table[i].media_offset = header.section_table[i-1].media_end;
		
		fseek(section, 0, SEEK_END);
		uint64_t section_size = ftell(section);
		
		nca_inject_pfs0(&header, i,
				&cr_callback_write_stdio, nca,
				&cr_callback_read_stdio, section, section_size);
		
		fclose(section);
	}
	
	fseek(nca, 0, SEEK_END);
	header.filesize = ftell(nca);
	
	char enc_hdr[ sizeof(nca_header_t) ];
	nca_encrypt_header(enc_hdr, &header);
	fseek(nca, 0, SEEK_SET);
	fwrite(enc_hdr, 1, sizeof(nca_header_t), nca);
	fclose(nca);
}
