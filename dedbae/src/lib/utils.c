#include "utils.h"

#include <stdlib.h>
#include <stdio.h>

#include "sha.h"

// Safe to assume we should have 1GB of memory to play with... right?
#define BYTES_PER_CYCLE 0x40000000

void hash_file_256(char* hash, baecb_read in, void* cbdata) {
    sha_ctx_t *sha_ctx = new_sha_ctx(HASH_TYPE_SHA256, 0);
    char* buf = malloc(BYTES_PER_CYCLE);
    uint64_t file_pos = 0;
    uint64_t readsize;

    do {
        readsize = (*in)(buf, file_pos, BYTES_PER_CYCLE, cbdata);
        sha_update(sha_ctx, buf, readsize);
        file_pos += BYTES_PER_CYCLE;
    } while(readsize > 0);

    sha_get_hash(sha_ctx, hash);
    free_sha_ctx(sha_ctx);
    free(buf);
}

void hash_buf_256(void* buf, char* hash, unsigned int buf_size) {
    sha_ctx_t *sha_ctx = new_sha_ctx(HASH_TYPE_SHA256, 0);
    sha_update(sha_ctx, buf, buf_size);
    sha_get_hash(sha_ctx, hash);
    free_sha_ctx(sha_ctx);
}
