#include "cnmt.h"

#include "utils.h"
#include "nca.h"
#include "types.h"
#include "xci2.h"

#include <string.h>
#include <stdio.h>
char* cnmt_content_type_to_string(uint8_t type) {
	switch( type ) {
		case CNMT_CONTENT_TYPE_META:
			return "Meta"; break;
		case CNMT_CONTENT_TYPE_PROGRAM:
			return "Program"; break;
		case CNMT_CONTENT_TYPE_DATA:
			return "Data"; break;
		case CNMT_CONTENT_TYPE_CONTROL:
			return "Control"; break;
		case CNMT_CONTENT_TYPE_MANUAL:
			return "Manual"; break;
		case CNMT_CONTENT_TYPE_LEGALINFO:
			return "Legalinfo"; break;
		case CNMT_CONTENT_TYPE_GAME_UPDATE:
			return "Game update"; break;
		default:
			return "INVALID";
	}
}

uint8_t cnmt_content_type_from_nca(uint8_t flag) {
    switch(flag) {
        case NCA_CONTENT_TYPE_PROGRAM:
            return CNMT_CONTENT_TYPE_PROGRAM;
        case NCA_CONTENT_TYPE_META:
            return CNMT_CONTENT_TYPE_META;
        case NCA_CONTENT_TYPE_CONTROL:
            return CNMT_CONTENT_TYPE_CONTROL;
        case NCA_CONTENT_TYPE_MANUAL:
            return CNMT_CONTENT_TYPE_LEGALINFO; // Usually the case, weirdly enough
        case NCA_CONTENT_TYPE_DATA:
            return CNMT_CONTENT_TYPE_DATA;
    }
}

void cnmt_init_header(cnmt_header_t* hd, cnmt_header_t* old_hd) {
	memset(hd, 0, sizeof(cnmt_header_t));

	if(old_hd != NULL) {
		hd->titleid = old_hd->titleid;
		hd->version = old_hd->version;
		hd->title_type = old_hd->title_type;

		if(old_hd->table_offset >= 0x10) // There is a titletype-specific header
			// Doesn't matter which; they're all the same size
			memcpy(&hd->app, &old_hd->app, sizeof(cnmt_application_header_t));

	}
	else
		hd->title_type = TITLE_TYPE_APPLICATION;

	hd->table_offset = 0x10;
}

void cnmt_create_content_record(cnmt_content_record_t* out, baecb_read in, void* cbdata) {
	char enc_hdr[ sizeof(nca_header_t) ];
	nca_header_t nca_header;
	(*in)(enc_hdr, 0, sizeof(nca_header_t), cbdata);
	fprintf(stdout, "decryptingheader\n");
	nca_decrypt_header(&nca_header, enc_hdr);
	fprintf(stdout, "decryptedheader\n");
	// Hash and NCAID

	hash_file_256(out->hash, in, cbdata);
	fprintf(stdout, "hashedfile\n");
	memcpy(out->ncaid, out->hash, 16);
	char hufhefhiu[33];
	hexBinaryString(out->hash, 16, hufhefhiu, 33);
	fprintf(stdout, "hash = %s\n", hufhefhiu);
	// File size
	fprintf(stdout, "hashedfile\n");
	cnmt_write_contentsize(nca_header.filesize, out);
	fprintf(stdout, "hashedfile\n");
	// Type
	out->type = cnmt_content_type_from_nca(nca_header.content_type);
	fprintf(stdout, "hashedfile\n");
}
