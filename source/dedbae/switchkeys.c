#include "switchkeys.h"

#include "utils.h"

#include "minIni/minIni.h"
#include "ini.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define DEFAULT_KEYFILE "sdmc:/prod.keys"
#define DEFAULT_KEYFILE_NOHOME "sdmc:/prod.keys"

switchkeys_t switchkeys;

// ishex(), hextoi(), parse_hex_key() taken from hactool © SciresM
static int ishex(char c) {
    if ('a' <= c && c <= 'f') return 1;
    if ('A' <= c && c <= 'F') return 1;
    if ('0' <= c && c <= '9') return 1;
    return 0;
}

static char hextoi(char c) {
    if ('a' <= c && c <= 'f') return c - 'a' + 0xA;
    if ('A' <= c && c <= 'F') return c - 'A' + 0xA;
    if ('0' <= c && c <= '9') return c - '0';
    return 0;
}

bool parse_hex_key(unsigned char *key, const char *hex, unsigned int len) {
    if (strlen(hex) != 2 * len) {
        //fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
        return false;
    }

    for (unsigned int i = 0; i < 2 * len; i++) {
        if (!ishex(hex[i])) {
            //fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
            return false;
        }
    }

    memset(key, 0, len);

    for (unsigned int i = 0; i < 2 * len; i++) {
        char val = hextoi(hex[i]);
        if ((i & 1) == 0) {
            val <<= 4;
        }
        key[i >> 1] |= val;
    }
    return true;
}

void import_key(char* keyfile, char* keyname, char* output, unsigned int size)
{
    char key_octets[2*size+1]; // +1 for trailing null
    fprintf(stdout,"before ini gets\n");
    ini_gets("", keyname, "", key_octets, 2*size+1, keyfile);
    fprintf(stdout, "key oc: %s\n", key_octets);
    fprintf(stdout,"after inigets\n");
    if(key_octets[0] == '\0') {
      fprintf(stdout, "keyfile hasnt got a key %s, exiting.....\n", keyname);
        // Keyfile does not have this key
        // TODO: Return error
        return;
    }

    parse_hex_key(output, key_octets, size);
}
static int handler(void* testconf, const char* section, const char* name,
                   const char* value)
{
    switchkeys_t* switchkeys = (switchkeys_t*)testconf;
    printf("handler uwu\nname: %s\n", name);
    if(!strcmp("header_key", name)) {
        printf("header_key OwO: %s\n", value);
        parse_hex_key(switchkeys->header, value, 32);
    } else if(!strcmp("key_area_key_application_00", name)) {
        printf("appkey1 OwO: %s\n", value);
        parse_hex_key(switchkeys->application_key_area[0], value, 16);
    } else if(!strcmp("key_area_key_application_01", name)) {
        printf("appkey2 OwO: %s\n", value);
        parse_hex_key(switchkeys->application_key_area[1], value, 16);
    } else if(!strcmp("key_area_key_application_02", name)) {
        printf("appkey3 OwO: %s\n", value);
        parse_hex_key(switchkeys->application_key_area[2], value, 16);
    } else if(!strcmp("key_area_key_application_03", name)) {
        printf("appkey4 OwO: %s\n", value);
        parse_hex_key(switchkeys->application_key_area[3], value, 16);
    } else if(!strcmp("key_area_key_application_04", name)) {
        printf("appkey5 OwO: %s\n", value);
        parse_hex_key(switchkeys->application_key_area[4], value, 16);
    } else if(!strcmp("titlekek_00", name)) {
      printf("titlekek1 OwO: %s\n", value);
      parse_hex_key(switchkeys->titlekeks[0], value, 16);
    } else if(!strcmp("titlekek_01", name)) {
      printf("titlekek2 OwO: %s\n", value);
      parse_hex_key(switchkeys->titlekeks[1], value, 16);
    } else if(!strcmp("titlekek_02", name)) {
      printf("titlekek3 OwO: %s\n", value);
      parse_hex_key(switchkeys->titlekeks[2], value, 16);
    } else if(!strcmp("titlekek_03", name)) {
      printf("titlekek4 OwO: %s\n", value);
      parse_hex_key(switchkeys->titlekeks[3], value, 16);
    } else if(!strcmp("titlekek_04", name)) {
      printf("titlekek5 OwO: %s\n", value);
      parse_hex_key(switchkeys->titlekeks[4], value, 16);
    }

    return 1;
}
void print_hex(unsigned char* key,  int key_length) {
    for(int i = 0; i < key_length; i++)
        fprintf(stdout,"%02x", key[i]);
    fprintf(stdout,"\n");
}
void switchkeys_load_keyfile(char* specified_keyfile) {
    char* rel_keyfile = specified_keyfile;

    if(rel_keyfile == NULL) {
      fprintf(stdout, "No keys file found\n");
        char* homedir = getenv("HOME");
        if(homedir == NULL) homedir = getenv("HOMEPATH"); // Because Windows just *has* to be different.
        if(homedir != NULL) {
            fprintf(stdout, "homedir exists... %s\n", homedir);
            rel_keyfile = malloc( strlen(homedir) + strlen(DEFAULT_KEYFILE) + 1 );
            strcpy(rel_keyfile, homedir);
            strcat(rel_keyfile, DEFAULT_KEYFILE);
        }
        else rel_keyfile = DEFAULT_KEYFILE_NOHOME;
    }

    char* keyfile = rel_keyfile;
    if(keyfile == NULL){
        fprintf(stdout, "no keyfile\n");
        // TODO: Return error
        return;
      }
    fprintf(stdout, "Starting key import\n");
    if (ini_parse("sdmc:/prod.keys", handler, &switchkeys) < 0) {
        printf("Can't load 'test.ini'\n");
        return 1;
    } else {
      fprintf(stdout, "keys loaded\nheader key: ");
      print_hex(switchkeys.header, 32);
      fprintf(stdout, "\napp keys 0-4:\n");
      print_hex(switchkeys.application_key_area[0], 16);
      print_hex(switchkeys.application_key_area[1], 16);
      print_hex(switchkeys.application_key_area[2], 16);
      print_hex(switchkeys.application_key_area[3], 16);
      print_hex(switchkeys.application_key_area[4], 16);
    }
    fprintf(stdout, "keys loaded \n");
    //free(keyfile);
}
