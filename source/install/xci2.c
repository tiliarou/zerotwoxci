/*
	Written by elise
	https://twitter.com/EliseZeroTwo
*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include "cnmt.h"
#include "xci2.h"
#include "switchkeys.h"
#include "../dedbae/src/lib/utils.h"
#include "nca.h"
#include "debugger.h"
#include "types.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/errno.h>
#include <unistd.h>
#include <switch.h>
#define onegb 0x40000000
#define HEADER_SIZE 3072 // 0xc00
#define thmb 209715200
#include <dirent.h>
int reinitdir() {
	fprintf(stdout, "called\n");
	DIR *theFolder = opendir("sdmc:/tinfoil/zerotwoxci/ncas");
 	struct dirent *next_file;
 	char filepath[256];
	while ( (next_file = readdir(theFolder)) != NULL )
	{
			 // build the path for each file in the folder
			sprintf(filepath, "%s/%s", "sdmc:/tinfoil/zerotwoxci/ncas", next_file->d_name);
			fprintf(stdout, "removing: %s\n", filepath);
			remove(filepath);
	}
 	closedir(theFolder);
	return 1;
}
uint64_t callback_read_stdio(char* buf, uint64_t offset, uint64_t size, void* data) {
	FILE* fd = data;
	fseek(fd, offset, SEEK_SET);
	fread(buf, 1, size, fd);
}
uint64_t callback_write_stdio(char* buf, uint64_t offset, uint64_t size, void* data) {
	FILE* fd = data;
	fseek(fd, offset, SEEK_SET);
	fwrite(buf, 1, size, fd);
}
char hexTab[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
'8', '9', 'a', 'b', 'c', 'd', 'e', 'f', };
void hextostrings(char * in, char * out) {
	for (int i = 0; i < strlen(in); i++) {

	}
}
void hexBinaryString(unsigned char *in, int inSize, char *out, int outSize) {
while (--inSize >= 0)
    {
    unsigned char c = *in++;
    *out++ = hexTab[c>>4];
    *out++ = hexTab[c&0xf];
    }
*out = 0;
}
void cnmt_init_header2(cnmt_header_t* hd, char* oldfile) {
	memset(hd, 0, sizeof(cnmt_header_t));

	if(oldfile != NULL) {
		cnmt_header_t old_hd;
		memcpy(&old_hd, oldfile, sizeof(cnmt_header_t));
		//fclose(oldfile);

		hd->titleid = old_hd.titleid;
		hd->version = old_hd.version;
		hd->title_type = old_hd.title_type;

		if(old_hd.table_offset >= 0x10) // There is a titletype-specific header
			// Doesn't matter which; they're all the same size
			memcpy(&hd->app, &old_hd.app, sizeof(cnmt_application_header_t));

	}
	else
		hd->title_type = TITLE_TYPE_APPLICATION;

	/* TODO: Inject cmdline-specified:
	 * titleid
	 * version
	 * titletype
	 * titletype-specific header values
	 */

	hd->table_offset = 0x10;
}
void cnmt_builder2(cnmt_content_record_t records[], hfs0_ctx_t *xci, FILE * cnmtnca, int numfiles) {
  //Variables used throughout build
  nca_header_t ncaheader;
  char encryptedheader[0xc00];
  cnmt_header_t cnmtheader;
  FILE * rawpfso = fopen("/rawpfso.zerotwo", "w+b");
  uint8_t lengthofrawfile;
  char * oldcnmt;
  char shahash[32];
  char shahashoftheshahash[32]; //dont ask
  uint64_t lengthofpfso;
  char * finishedpfs0;
  char * finalsec;
  char idkanymore[32];
  char encheaderfinal[0xc00];
  char sectionblock[0x200];
  char sectionblockhash[32]; //thats a lot of hashes
  //end of variables
  fseek(cnmtnca, 0, SEEK_SET);
  fread(encryptedheader, 1, 0xc00, cnmtnca);
  nca_decrypt_header(&ncaheader, encryptedheader);
  nca_extract_section(&ncaheader, 0, callback_write_stdio, rawpfso, callback_read_stdio, cnmtnca);
	fprintf(stdout, "Extracted section\n");
  fseek(rawpfso, 0x38, SEEK_SET);
  fread(&lengthofrawfile, 1, 1, rawpfso);
  oldcnmt = (char*)malloc(lengthofrawfile);
  fseek(rawpfso, 0x80, SEEK_SET);
  fread(oldcnmt, 1, lengthofrawfile, rawpfso);
  cnmt_init_header2(&cnmtheader, oldcnmt);
  fseek(rawpfso, 0xB0, SEEK_SET);
  for (int x = 0; x < numfiles; x++) {
		char endstring[33];
		hexBinaryString(records[x].hash, 16, endstring, 33);
    fwrite(&records[x],1,sizeof(records[x]),rawpfso);
  }
  fseek(rawpfso,0,SEEK_END);
  lengthofpfso = ncaheader.section_headers[0].pfs0.fs_size;
  finishedpfs0 = malloc(lengthofpfso);
  fseek(rawpfso, 0x20, SEEK_SET);
  fread(finishedpfs0, 1, lengthofpfso, rawpfso);
  hash_buf_256(finishedpfs0, shahash, lengthofpfso);
  fseek(rawpfso, 0, SEEK_SET);
  fwrite(shahash, 1, 32, rawpfso);
  fseek(rawpfso, 0,SEEK_SET);
  fread(idkanymore, 1 ,32 ,rawpfso);
  hash_buf_256(idkanymore, shahashoftheshahash, 32);
  memcpy(&ncaheader.section_headers[0].pfs0.master_hash, &shahashoftheshahash, 32);
  memcpy(&sectionblock, &ncaheader.section_headers[0] , 0x200);
  hash_buf_256(sectionblock, sectionblockhash, 0x200);
  memcpy(&ncaheader.hashes[0], &sectionblockhash, 32);
  nca_encrypt_header(encheaderfinal, &ncaheader);
  fseek(cnmtnca, 0, SEEK_SET);

  fwrite(encheaderfinal, 1, 0xc00, cnmtnca);
	fprintf(stdout, "replacing section\n");
  nca_replace_section(&ncaheader, 0, callback_write_stdio, cnmtnca, callback_read_stdio, rawpfso);
	fprintf(stdout, "replacing section\n");
}

//Technically main but im loading it ontop of tinfoil and gotta call it
int startxcidecompile (char * filepath)
{
  FILE *file = fopen(filepath, "rb");
  if (file == NULL) {
    fprintf(stderr, "Cant open game.xci...");
    return 1;
  }
  // Seek to the end to get length of file
  fseek(file, 0, SEEK_END);
  unsigned int length = ftell(file);
  xci_ctx_t xcifile;
  memset(&xcifile, 0, sizeof(xcifile));
  xcifile.file = file;
  fseek(file, 0, SEEK_SET);
	fprintf(stdout,"keys gonna load\n");
  switchkeys_load_keyfile(NULL); //here
	fprintf(stdout,"keysloaded\n");
  xciprocess(&xcifile);
  fclose(file);
	return 0;
}

void xciprocess(xci_ctx_t *xci) {
  if (xci->file == NULL) {
    fprintf(stderr, "FiLe CoRruPT or smth we just cant read it :(\nexiting...\n");
    return;
  }
  fseek(xci->file, 0, SEEK_SET);
  if(fread(&(xci->header), 1, 0x200, xci->file) != 0x200){
    fprintf(stderr, "XCI header was corrupt\n");
    return;
  }
  if(strncmp((char*)&(xci->header.magic), "HEAD", 4)) {
    fprintf(stderr, "xci header corrupt. magic was wrong");
    return;
  }
	fprintf(stdout, "xci header valid\n");
  xci->partition_ctx.offset = 0xF000;
  xci->partition_ctx.file = xci->file;
  xci->partition_ctx.name = "root";
  hfs0_header_t header;
  fseek(xci->file, 0xF000, SEEK_SET);
  fread(&header, 1, sizeof(header), xci->file);
  fseek(xci->file, 0, SEEK_SET);
  //mkdir("./output", 0755);
  for (int i = 0; i < header.num_files; i++) {
    hfs0_file_entry_t * file = hfs0_get_file_entry(&header, i);
    if (i == 2)
      {
        fseek(xci->file, 0xF090, SEEK_SET);
        fread(&(xci->secure_ctx.offset), 1, sizeof(xci->secure_ctx.offset), xci->file);
        xci->secure_ctx.file = xci->file;
        xci->secure_ctx.name = "secure";
        mkdir("./tinfoil/zerotwoxci/ncas", 0755);
        hfs0process(&(xci->secure_ctx));
      }
    }
  }

void hfs0process (hfs0_ctx_t *xci) {
  hfs0_header_t header;
  fseek(xci->file, 0xF200, SEEK_SET);
  fseek(xci->file, xci->offset, SEEK_CUR);
  fread(&header, 1, sizeof(header), xci->file);
  if(strncmp((char*)(&header.magic), "HFS0", 4)) {
    fprintf(stderr, "HSF0 header corrupt but at offset of %lu\n", (xci->offset+0xF200));
    return;
  }
  fseek(xci->file, 0xF200, SEEK_SET);
  fseek(xci->file, xci->offset, SEEK_CUR);
  uint64_t headersize = hfs0_get_header_size(&header);
  xci->header = malloc(headersize);

  if(fread(xci->header, 1, headersize, xci->file) != headersize) {
    fprintf(stderr, "header corrupt\n");
    return;
  }
  hfs0have(xci, 0);
}


void hfs0have (hfs0_ctx_t *xci, uint8_t x) {
  fseek(xci->file, 0xF200, SEEK_SET);
  fseek(xci->file, xci->offset, SEEK_CUR);
  uint32_t *offsets = (uint32_t*) malloc(xci->header->num_files * sizeof(uint32_t));
  for (int i = 0; i < xci->header->num_files; i++) {
    fseek(xci->file, 0x10, SEEK_CUR);
    fread(&(offsets[i]), 1, 0x4, xci->file);
    fseek(xci->file, xci->offset, SEEK_SET);
    fseek(xci->file, (i+1)*0x40, SEEK_CUR);
  }
  size_t n = xci->header->num_files-1;
  cnmt_content_record_t records[3];
  FILE * cnmtfile = NULL;
  char * cnmtfilepath = NULL;
  uint8_t num_files = xci->header->num_files;
  int partnum = 0;
  for (int i = 0; i < num_files; i++) {
    char * output = NULL;
    char * filename = NULL;
    char cnmtmagic[4] = { 0 };
    char namebuf[0x100] = { 0 };
    char filedestination[0x40] = { 0 };
    char filen[0x20] = { 0 };
    char shafile[0x20] = { 0 };
    char actualname[0x21] = { 0 };
    char * finalname;
    hfs0_file_entry_t * file = hfs0_get_file_entry(xci->header, i);
    fseek(xci->file, 0xF200, SEEK_SET);
    fseek(xci->file, xci->offset, SEEK_CUR);
    fseek(xci->file, 0x10, SEEK_CUR);
    fseek(xci->file, xci->header->num_files*0x40, SEEK_CUR);
    fseek(xci->file, xci->header->string_table_size, SEEK_CUR);
    fseek(xci->file, file->offset, SEEK_CUR);
    fprintf(stdout, "tranferring data... this may take a while so do not worry if it seems frozen\n");
		uint64_t remainingsize = file->size;
		snprintf(namebuf, 0x100, "sdmc:/tinfoil/zerotwoxci/ncas/%s", hfs0_get_file_name(xci->header, i));
		FILE * outputfile = fopen(namebuf, "w+b");
		if (outputfile == NULL) {
			fprintf(stderr, "Output file null...\n exiting...\n");
			exit(0);
		}
		for (int i = thmb; i < remainingsize; remainingsize=remainingsize-thmb){
			output = malloc(thmb);
			if (!output) {
				fprintf(stderr, "malloc failed\n");
				return;
			}
			fread(output, 1, thmb, xci->file);
			fwrite(output , 1 , thmb , outputfile);
			free(output);
		}
		output = malloc(remainingsize);
		if (!output) {
			fprintf(stderr, "malloc failed\n");
			return;
		}
		fprintf(stdout, "finished tranferring...\n");
		fread(output, 1, remainingsize, xci->file);
		fwrite(output , 1 , remainingsize , outputfile);
		fseek(xci->file, xci->offset+0x10, SEEK_SET);
		fseek(xci->file, (xci->header->num_files*0x40)+file->string_table_offset, SEEK_CUR);
		fread(filen, 1, 0x30, xci->file);
    filename = hfs0_get_file_name(xci->header, i);
    memcpy(cnmtmagic, &filename[strlen(filename)-8], 4);


    nca_header_t nca_header;
    char encryptedheadernca[0xc00];
    fseek(outputfile, 0, SEEK_SET);
    fread(encryptedheadernca, 1, 0xc00, outputfile);
    nca_decrypt_header(&nca_header, encryptedheadernca);
    nca_header.location = 0;
    nca_encrypt_header(encryptedheadernca, &nca_header);
    fseek(outputfile,0,SEEK_SET);
    fwrite(encryptedheadernca, 1,0xc00, outputfile);
    if (strncmp(cnmtmagic, "cnmt", 4) == 0) {
      cnmt_builder2(records, xci, outputfile,3);
      free(output);
      fclose(outputfile);
      return;
    }


		fprintf(stdout, "creating cnmt record... this may take a while...\n");
		fseek(outputfile, 0, SEEK_SET);
    cnmt_create_content_record(&records[i], callback_read_stdio, outputfile);
		char ehiruwhruiwefh[33]; //i hate life
		hexBinaryString(records[i].hash, 16, ehiruwhruiwefh, 33);
		fprintf(stdout, "created record!\n");
    memcpy(&shafile, &records[i-partnum].hash, 16);
  	hexBinaryString(shafile, strlen(shafile), actualname, 32);
		finalname = (char*)malloc(strlen("sdmc:/tinfoil/zerotwoxci/ncas/.nca")+strlen(ehiruwhruiwefh)+1);
    snprintf(finalname, strlen("sdmc:/tinfoil/zerotwoxci/ncas/.nca")+strlen(ehiruwhruiwefh)+1, "sdmc:/tinfoil/zerotwoxci/ncas/%s.nca", ehiruwhruiwefh);
    fclose(outputfile);
    int ret = rename(namebuf, finalname);
    free(output);
  }
}
