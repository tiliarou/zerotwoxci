#pragma once

#include <stdint.h>

// Callback used by some CNMT and NCA functions when the work area is too large for a simple memory buffer.
// These will be called with the buffer to read/write, at what offset in the file, how many bytes to read, and a
// pointer value of your choosing.
typedef uint64_t (*baecb_read)(char* buf, uint64_t offset, uint64_t size, void* data);
typedef uint64_t (*baecb_write)(char* buf, uint64_t offset, uint64_t size, void* data);

/* An example using stdio:
uint64_t baecb_read_stdio(char* buf, uint64_t offset, uint64_t size, void* data) {
	FILE* fd = data;
	fseek(fd, offset, SEEK_SET);
	fread(buf, 1, size, fd);
}
*/
